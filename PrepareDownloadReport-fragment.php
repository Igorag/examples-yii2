<?php

namespace app\services;

use app\models\Answers;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Writer\Xls;

class PrepareDownloadReport
{

    /**
     * Получение данных для отчета - результат поиска
     * @param String $idString
     * @return array
     */
    public static function queryForReportPurchaseBySearch($answersIdString)
    {
        $sqlAnswers = "
            select
                content->>'дата_время' as date,
                public.user.fullname as fullname,
                cast(content->>'номер' as integer) as pharmacy_number,
                replace(content->>'сумма', ',', '.') as summ,
                personal,
                sale_amount,
                notes->>'sale_amount' as sale_amount_note
            from answers
            left join public.user
            on answers.user_id=public.user.id
            where
                answers.id in (".$answersIdString.")
            order by pharmacy_number
        ";
        $answersStat = Answers::findBySql($sqlAnswers)->asArray()->all();

        //
        return $answersStat;
    }

    /**
     * Запрос-выборка для сводного отчета - план проверки
     * @param String $idString
     * @return array
     */
    public static function queryForReportUnitedByPlan($idString)
    {
        // Запрос-общий-все данные
        $sqlMoney="
            select
                buyer_id,
                fullname,
                buyer_amount,
                user_amount_before_payment,
                dt_first_payment,
                user_amount_before_purchase,
                dt_first_purchase,
                summ_payments_deleted,
                sum_amount,
                expenses,
                personal_expenses,
                check_count,
                plan_id_purshase,
                note
            from
            (
                select
                    buyer_id
                from
                    plan_calendar,
                    json_to_recordset(plan_data) as x(rounds_data json),
                    json_to_recordset(rounds_data) as y(first_round json, second_round json),
                    cast(first_round->>'buyer_id' as int) as buyer_id,
                    cast(second_round->>'buyer_id' as int) as buyer_id2
                where id in (".$idString.")
                group by buyer_id
            ) as buyers

            -- Текущий баланс покупателя
            left join
                (select id, amount as buyer_amount from public.user) as users
            on users.id = buyers.buyer_id

            -- Самый ранний платеж
            left join
            (
                select user_id_to_payment, id, dt_first_payment, user_amount_before_payment, plan_id_payment
                from
                (
                    select
                        user_id_to as user_id_to_payment,
                        id,
                        plan_id as plan_id_payment,
                        created_at as dt_first_payment,
                        prev_user_amount as user_amount_before_payment,
                        rank() over (partition by user_id_to order by created_at asc)
                    from payments
                     where plan_id in (".$idString.")
                              and payment_type = ".Payments::PAYMENT_TYPE_PLUS."
                              and status <> ".Payments::PAYMENT_DELETED."
                ) as payments_data_purschase
                where rank = 1
            ) as amounts_before_payment
            on buyers.buyer_id = amounts_before_payment.user_id_to_payment

            -- Самая ранняя покупка\проверка\анкета (из платежей)
            left join
            (
                select user_id_from, id, dt_first_purchase, user_amount_before_purchase, plan_id_purshase
                from
                (
                    select
                        user_id_from,
                        id,
                        plan_id as plan_id_purshase,
                        created_at as dt_first_purchase,
                        prev_user_amount as user_amount_before_purchase,
                        rank() over (partition by user_id_to order by created_at asc)
                    from payments
                     where plan_id in (".$idString.")
                              and payment_type = ".Payments::PAYMENT_TYPE_CHECK."
                ) as payments_data_purschase
                where rank = 1
            ) as amounts_before_purchase
            on buyers.buyer_id = amounts_before_purchase.user_id_from

            -- Сумма удаленных платежей в разрезе покупателей
            -- только те, которые удалены после заполнения првой анкеты
            -- (платеж за анкету - created, платеж - updated)
            left join
                ( select user_id_to as user_id_to_deleted, sum(amount) as summ_payments_deleted
                  from payments
                  where plan_id in (".$idString.")
                           and status = ".Payments::PAYMENT_DELETED."
                           and created_at>
                               (select created_first_purchase from
                                   (select
                                        created_at as created_first_purchase,
                                        rank() over (partition by user_id_to order by created_at asc)
                                    from  payments
                                    where plan_id in (".$idString.")
                                             and status <> ".Payments::PAYMENT_DELETED."
                                             and payment_type = ".Payments::PAYMENT_TYPE_CHECK."
                                   ) as payments_data_purschase_deleted
                                where rank = 1
                               )
                  group by user_id_to) as payments_deleted
              on buyers.buyer_id = payments_deleted.user_id_to_deleted

            -- Полное имя покупателя
            left Join
                (select id, fullname from public.user) as fullnames
            on buyers.buyer_id = fullnames.id

            -- Выплаты покупателям для совершения покупок
            left join
            (
                select user_id_to, sum(amount) as sum_amount
                 from payments
                 where plan_id in (".$idString.")
                          and payment_type=".Payments::PAYMENT_TYPE_PLUS."
                          and status <> ".Payments::PAYMENT_DELETED."
                 group by user_id_to
            ) as amounts
              on buyers.buyer_id = amounts.user_id_to

            -- Затраты на покупки
            left join
            (
                select
                    user_id,
                    sum(cast(replace(content->>'сумма', ',', '.') as numeric)) as expenses,
                    count(*)as check_count
                from answers
                where plan_id in (".$idString.")
                      and answers.status not in (".Answers::ANSWER_DRAFT.",".Answers::ANSWER_DELETED.")
                group by user_id
            ) as answers_data
              on buyers.buyer_id = answers_data.user_id

            -- Сумма личных покупок
            left join
              (
                  select user_id, sum(cast(replace(content->>'сумма', ',', '.') as numeric)) as personal_expenses
                  from answers
                  where plan_id in (".$idString.")
                      and answers.status not in (".Answers::ANSWER_DRAFT.",".Answers::ANSWER_DELETED.")
                      and personal = true
                  group by user_id
              ) as answers_data_pesonal
              on buyers.buyer_id = answers_data_pesonal.user_id

             -- Примечания
            left join
            (
                select user_id_to, string_agg(note, ',') as note
                from payments
                where plan_id in (".$idString.")
                group by user_id_to
            ) as notes
            on buyers.buyer_id = notes.user_id_to
        ";

        //
        $amounts = Payments::findBySql($sqlMoney)->asArray()->all();

        //
        return $amounts;
    }

    // Другие функции
    // ...

}
