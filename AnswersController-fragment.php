<?php

namespace app\controllers;

use Yii;

use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
//
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
//
use app\models\Answers;
use app\models\AnswersSearch;
//
use app\models\Templates;
use app\models\User;
//
use app\models\Rules;
use app\models\RulesException;

//
use app\services\PrepareDownloadReport;

//
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Writer\Xls;


/**
 * AnswersController implements the CRUD actions for Answers model.
 */
class AnswersController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => [],
                'rules' => [
                    [
                        'actions' => ['create', 'index',  'error', 'gethints' ],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    //
                    [
                       'actions' => ['fill', 'delete',],
                       'allow' => true,
                       'roles' => ['@'],
                       'matchCallback' => function ($rule, $action) {
                           return User::isUserAdmin() || $this->checkAnswerOwnerOnLoad();
                       }
                    ],
                    //
                    [
                       'actions' => ['view',],
                       'allow' => true,
                       'roles' => ['@'],
                       'matchCallback' => function ($rule, $action) {
                           return User::isUserAdmin() || User::isUserManager() || $this->checkAnswerOwnerOnLoad();
                       }
                    ],
                    //
                    [
                       'actions' => ['saveanswer', ],
                       'allow' => true,
                       'roles' => ['@'],
                       'matchCallback' => function ($rule, $action) {
                           return User::isUserAdmin() || $this->checkAnswerOwner();
                       }
                    ],
                    //
                    [
                       'actions' => [
                           'restore',
                           //
                           'setpersonal',
                           'saleamount',
                           //
                           'downloadexpensesbypurchaseviasearch',
                           'downloadexpensesbyphamacyviasearch',
                       ],
                       'allow' => true,
                       'roles' => ['@'],
                       'matchCallback' => function ($rule, $action) {
                           return User::isUserAdmin() ;
                       }
                    ],
                    //
                    [
                       'actions' => ['unloadcsv', 'unloadxls', 'unloadreportcsv', 'unloadreportxls'],
                       'allow' => true,
                       'roles' => ['@'],
                       'matchCallback' => function ($rule, $action) {
                           return User::isUserAdmin() || User::isUserManager() ;
                       }
                    ],
                ],
            ],
            //
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Проверка -  должна редактироваться и заполняться "своя" запись
     * @return boolean
     */
    protected function checkAnswerOwnerOnLoad()
    {
        return Answers::findOne(Yii::$app->request->get('id'))->user_id == Yii::$app->user->id;
    }

    /**
     * Проверка -  должна сохраняться "своя" запись
     * @return boolean
     */
    protected function checkAnswerOwner()
    {
        return Answers::findOne(Yii::$app->request->post('id'))->user_id == Yii::$app->user->id;
    }

    // ...
    // ...

}