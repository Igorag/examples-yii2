Пример кода  (из "пробного" проекта - работа с данными ЦБ РФ, курсы валют)
====================================================

UpdatecurrencyController.php
консольный контроллер для получения данных. Или через curl или с использованием HttpClient - расширения Yii2
 
CurrencyController.php - контроллер, "отдающий" данные о курсах валют (REST full).

Пример кода (запросы)
=================

PrepareDownloadReport-fragment.php - фрагмент класса PrepareDownloadReport, для подготовки выгрузки данных

queryForReportPurchaseBySearch() - функция с обращением к данным "внутри" json-структуры

queryForReportUnitedByPlan() - функция, выбирающая данные для одного из отчетов, в запросе используются так называемые оконные функции PostgreSQL, например, такой подзапрос
                    select
                        user_id_to as user_id_to_payment,
                        id,
                        plan_id as plan_id_payment,
                        created_at as dt_first_payment,
                        prev_user_amount as user_amount_before_payment,
                        rank() over (partition by user_id_to order by created_at asc)
                    from payments
                     where plan_id in (".$idString.")
                              and payment_type = ".Payments::PAYMENT_TYPE_PLUS."
                              and status <> ".Payments::PAYMENT_DELETED."

Пример кода - фрагмент модели
=====================
Фрагмент модели:. константы, behaviors

Пример кода -фрагмент контроллера
========================

behaviors - ограничение доступа
                                                            
Пример кода  (примеры простых тестов, Yii2, Codeception)
=====================================
Чтобы показать, что тестирование тоже знакомо практически - "пара" примеров.

LoginCept.php - acceptance (приемочный) тест
NoticelistCept.php - функциональный тест

Пример кода -pl/pgsql
==============

pl-pgsql-fragment.sql - содержит пример собственного типа дыннх для PostgreSQL и пример функции на pl/pgsq
