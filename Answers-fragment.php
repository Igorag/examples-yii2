<?php

namespace app\models;

use Yii;
use yii\helpers\Json;
//
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "answers".
 *
 * @property string $id
 * @property string $user_id
 * @property string $template_id
 * @property string $template_name
 * @property string $template
 * @property integer $status
 * @property string $content
 * @property string created_at
 * @property string updated_at
 * @property string sale_amount
 * @property boolean personal
 * @property boolean blocked
 * @property string $notes
 */
class Answers extends \yii\db\ActiveRecord
{
    /**
     * @var integer ANSWER_DRAFT Константа для статуса "черновик"
     */
    const ANSWER_DRAFT = 0;

    /**
     * @var integer ANSWER_OK Константа для статуса "анкета заполнена"
     */
    const ANSWER_OK= 2;

    /**
     * @var integer ANSWER_RATING_DRAFT Константа для статуса "рейтинги не заполнены"
     *
     */
    const ANSWER_RATING_DRAFT= 3;

    /**
     * @var integer ANSWER_RATING_OK Константа для статуса "рейтинги заполнены"
     */
    const ANSWER_RATING_OK= 5;

    /**
     * @var integer ANSWER_DELETED Константа для статуса "удалено"
     */
    const ANSWER_DELETED = 100;

    // "Вспомогательные" статусы
    /**
     * @var integer ANSWER_DRAFT_BLOCKED_BY_TIME Константа для статуса блокировки по времени
     */
    const ANSWER_DRAFT_BLOCKED_BY_TIME = 200;

    /**
     * @var string URL_PHARMACIES_DATA Константа  - URL для получения списк аптек
     */
    const URL_PHARMACIES_DATA = '#';

    /**
     * @var string URL_PHARMACIES_HINTS Константа - URL для получения подсказок
     */
    const URL_PHARMACIES_HINTS = '';

    /**
     * @var datetime CHECK_START_DATETIME Константа - момент начала проверок
     */
    const CHECK_START_DATETIME = '01.08.2017 00:00:00';

    /**
     * @var string $foundErrors[] Свойство-массив для "сбора" ошибок
     */
    public $foundErrors;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'answers';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        if (Yii::$app->controller->action->id == 'index')
            return [
                [
                    'class' => TimestampBehavior::className(),
                ]
            ];
            else
                return [
                    [
                        'class' => TimestampBehavior::className(),
                        'createdAtAttribute' => 'created_at',
                        'updatedAtAttribute' => 'updated_at',
                        'value' => function () {
                            return date('Y-m-d H:i:s');
                        }
                    ]
                ];
    }

    // ...
    // ...
}