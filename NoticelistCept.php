<?php

use tests\codeception\_pages\LoginPage;
use tests\codeception\_pages\NoticelistPage;

/* @var $scenario Codeception\Scenario */

$I = new FunctionalTester($scenario);
$I->wantTo('ensure that noticelist works');

$loginPage = LoginPage::openBy($I);
$I->amGoingTo('try to login with correct credentials');
$loginPage->login('admin', 'admin1');

$noticelistPage = NoticelistPage::openBy($I);
$I->see('Noticelists', 'h1');

$I->seeLink('About');
$I->seeLink('Пользователи');
