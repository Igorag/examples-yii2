<?php

namespace app\controllers;

use Yii;
use app\models\Currency;
//
use yii\rest\ActiveController;
use yii\data\ActiveDataProvider;
use yii\filters\auth\HttpBearerAuth;

use yii\web\Response;
use yii\helpers\ArrayHelper;

/**
 * CurrencyController implements the CRUD actions for Currency model.
 */
class CurrencyController extends ActiveController
{
    public $modelClass = 'app\models\Currency';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'only' => ['view', 'index'],
                'formats' => [
                    'application/html' => Response::FORMAT_HTML,
                ],
                'languages' => [
                    'en',
                    'ru',
                ],
            ],
/*            'authenticator' => [
                'class' => HttpBearerAuth::className(),
                'only' => ['view', 'index'],
            ],
*/
        ]);
    }

    /**
     * Unset some actions
     */
    public function actions()
    {
        $actions = parent::actions();
        unset($actions['index']);
        unset($actions['view']);

        return $actions;
    }

    /**
     * Lists all Currency models.
     * @return mixed
     */
    public function actionIndex()
    {
        $currenciesProvider = new ActiveDataProvider([
            'query' => Currency::find(),
                'pagination' => [
                    'pageSize' => 3
                ],
        ]);

        return $this->render('index', [
            'currenciesProvider' => $currenciesProvider
        ]);
    }

    /**
     * Displays a single Currency model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $currency = $this->findModel($id);

        return $this->render('view', [
            'currency' => $currency
        ]);
    }

    /**
     * Finds the Currency model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Currency the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Currency::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
