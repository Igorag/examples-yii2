<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;
use yii\console\ExitCode;

use yii\httpclient\Client;
use app\models\Currency;

/**
 * @author Игорь Герасимов <iagmail@mail.ru>
 */
class UpdatecurrencyController extends Controller
{
    /**
    *  Вставка (обновление) данных (HttpClient)
    * @return int Exit code
    */
    public function actionUpdatebyclient()
    {
        echo 'Update/insert currencies...'. "\n";

        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('post')
            ->setUrl('http://www.cbr.ru/scripts/XML_daily.asp')
            ->setData(['date_req' => $this->getCurrentDate()])
            ->send();
        //
        if ($response->isOk) {
            $data = $response->data;
            foreach ($data['Valute']  as $currencyCbrData)
               $this->saveCurrencyClient($currencyCbrData);
        } else echo  'problem, try again'. "\n";

        //
        return ExitCode::OK;
    }

    /**
    *  Вставка (обновление) данных (Curl)
    * @return int Exit code
    */
    public function actionUpdatebycurl()
    {
        echo 'Update/insert currencies...'. "\n";

        //
        $ch = curl_init('http://www.cbr.ru/scripts/XML_daily.asp');

        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        $headers =[];
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, array('date_req' => $this->getCurrentDate()));

        $resCurl = curl_exec($ch);
        curl_close($ch);

        //
        if ($resCurl) {
            $xmlCurrency = simplexml_load_string($resCurl);

            foreach ($xmlCurrency  as $currencyCbrXmlData)
                $this->saveCurrencyCurl($currencyCbrXmlData);
        } else  echo  'problem, try again'. "\n";

        //
        return ExitCode::OK;
    }

    /**
     * Сохраняет\обновляет данные курсов валют (Curl)
     * @param object $currencyCbrXmlData
     * @return boolean
    */
    protected function saveCurrencyCurl($currencyCbrXmlData)
    {
        $currency = Currency::findOne(''.$currencyCbrXmlData->CharCode);
        //
        if (is_null($currency)) {
            $currency = new Currency();
            //
            $currency->id = (string)$currencyCbrXmlData->CharCode;
            $currency->name = (string)$currencyCbrXmlData->Name;
         }

         //
         $rate = $currencyCbrXmlData->Value/$currencyCbrXmlData->Nominal;
         $currency->rate = $rate;

         //
         $currency->save();

         //
         return true;
    }

    /**
     * Сохраняет\обновляет данные курсов валют (HttpClient)
     * @param array $currencyCbrData
     * @return boolean
    */
    protected function saveCurrencyClient($currencyCbrData)
    {
        $currency = Currency::findOne($currencyCbrData['CharCode']);
        if (is_null($currency)) {
            $currency = new Currency();
            //
            $currency->id = $currencyCbrData['CharCode'];
            $currency->name = $currencyCbrData['Name'];
         }

         //
         $rate = $currencyCbrData['Value']/$currencyCbrData['Nominal'];
         $currency->rate = $rate;

         //
         $currency->save();
         //
         return true;
    }

    /**
     * Возвращает текущую дату в формате, который требует банк
     * @return string
     */
    protected function getCurrentDate()
    {
        $curDate = Date('d/m/Y');
        return $curDate;
    }

}
