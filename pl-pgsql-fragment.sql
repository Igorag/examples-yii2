-- Тип возврата для чеков возврата
----------------------------------
create type pb_schema.Tstat_get_return_receipt as (
       primary_key                           bigint,
       point_id                                 bigint,
       ip_address                             inet,
       return_receipt_id                   uuid,
       return_receipt_number          bigint,
       return_receipt_date_time       timestamp (3) with time zone,
       server_date_time                    timestamp (3) with time zone,
       amount                                  numeric(20,2),
       signature                               text,
       return_receipt_status             smallint,
       check_copy_on                       smallint,
       address                                  varchar
);

-- check_return_receipt
-----------------------
CREATE OR REPLACE FUNCTION pb_schema.check_return_receipt (varchar, varchar, out check_amount numeric)
   returns numeric as --
   $BODY$

   declare
      agent_id                         bigint;
      return_receipt_id_in       uuid;
      --
      agent_name                   varchar;
      qvr                                 varchar;

   begin
      select $1 into agent_id as bigint;
      select $2 into return_receipt_id_in as uuid;

      -- get agent name
      agent_name:=paybase_schema.get_agent_name(agent_id);

      -- select
      qvr:='select amount from pb_schema.Agent_Return_Receipts_'||agent_name||' where return_receipt_id=$1';
      -- execute
      execute qvr into check_amount using return_receipt_id_in;
      return;
   end;
   $BODY$
      LANGUAGE 'plpgsql' VOLATILE SECURITY DEFINER
      COST 100;
-- Privileges
ALTER FUNCTION pb_schema.check_return_receipt (varchar, varchar, out check_amount numeric) OWNER TO postgres;
REVOKE ALL ON FUNCTION pb_schema.check_return_receipt (varchar, varchar, out check_amount numeric) from public;
grant execute on function pb_schema.check_return_receipt (varchar, varchar, out check_amount numeric) to group group_request;

